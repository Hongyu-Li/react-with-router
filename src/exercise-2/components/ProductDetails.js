import React, {Component} from 'react';


class ProductDetails extends Component {

    render() {
        const {name, price, quantity, desc} = this.props.location.state;
        return (
            <div>
                <span>Name: {name}</span><br/>
                <span>Price: {price}</span><br/>
                <span>Quantity: {quantity}</span><br/>
                <span>Description: {desc}</span><br/>
            </div>
        );
    }
}

export default ProductDetails;