import React, {Component} from 'react';
import './products.css';
import {Link} from "react-router-dom";
import data from "../../exercise-2/mockups/data";


class Products extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = data;
    }

    render() {
        let products = [];
        for(var key in this.state){
            products.push(key);
        }
        return (
            <div>
                All Products:
                <ul>
                {products.map((item)=>{
                    const id = '/products:' + this.state[item].id;
                    return <Link to = {{
                        pathname: id,
                        state: this.state[item]}} ><li>{item}</li></Link>})
                }
                </ul>

            </div>
        );
    }
}

export default Products;