import React, {Component} from 'react';
import '../styles/App.css';
import Home from "./Home";
import {Redirect, Route, Switch} from "react-router";
import {BrowserRouter, Link} from "react-router-dom";
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";
import Products from "../../exercise-2/components/Products";
import ProductDetails from "../../exercise-2/components/ProductDetails";

class App extends Component {

    render() {
    return (
      <div className="app">
        <BrowserRouter>
          <header>
              <Link to = '/'>Home</Link>
              <Link to = '/products'>Products</Link>
              <Link to = '/my-profile'>MyProfile</Link>
              <Link to = '/about-us'>AboutUs</Link>
          </header>
          <Switch>
              <Route path = '/products' component ={Products}/>
              <Route path = '/goods' component ={Products}/>
              <Route path = '/my-profile' component ={MyProfile}/>
              <Route path = '/about-us' component ={AboutUs}/>
              <Route path='/products:id' component={ProductDetails}/>
              <Route path = '/' component ={Home}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
